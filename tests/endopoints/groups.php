<?php

use Advision\Fog\Authenticator;
use Advision\Fog\Client;

class GroupApiTest {

    public static function create($client)
    {
        $group = new \Advision\Fog\Models\Group();

        $group->setName('Gruppo '.rand(1,99))
                ->setSites([
                        'http://www.4dem.it',
                        'http://www.4marketing.it'
                    ]);

        return $client->save($group);
    }

    public static function update($client, $id, $data)
    {
        $group = \Advision\Fog\Factories\Group::fromArray($data);

        $group->setId($id);

        return $client->save($group);
    }

    public static function find($client, $id)
    {
        $group = \Advision\Fog\Factories\Group::fromArray([]);

        $group->setId($id);

        return $client->find($group);
    }

    public static function formsOfGroup($client, $id)
    {
        $group = \Advision\Fog\Factories\Group::fromArray([]);

        $group->setId($id)
              ->with('forms');

        return $client->find($group);
    }

    public static function delete($client, $id)
    {
        $group = \Advision\Fog\Factories\Group::fromArray([]);

        $group->setId($id);

        return $client->delete($group);
    }

}
///CREATE
/*try {

    $auth = new Authenticator(1223, $api_key, $api_password);

    $token = $auth->getToken();

    $client = new Client($token);

    $group = GroupApiTest::create($client);

    dd($group);

} catch (Exception $e) {
    dd($e->getMessage(), $e->getCode());
}*/
///// DELETE
/*try {

    $auth = new Authenticator(1223, $api_key, $api_password);

    $token = $auth->getToken();

    $client = new Client($token);

    $group = GroupApiTest::delete($client, "5975fc70a6dcf744dd59442f");

    dd($group);

} catch (Exception $e) {
    dd($e->getMessage(), $e->getCode());
}*/

///// FORMS OF GROUP
/*try {

    $auth = new Authenticator(1223, $api_key, $api_password);

    $token = $auth->getToken();

    $client = new Client($token);

    $group = GroupApiTest::formsOfGroup($client, "5976159298c93807044f2eab");

    dd($group);

} catch (Exception $e) {
    dd($e->getMessage(), $e->getCode());
}*/
// $r = createGroup($host, [
//     'auth_token' => $token,
//     'name' => 'gruppo_' . rand(1, 5),
//     'sites_allowed' => [
//         'http://test.it'
//     ]
// ]);
// dd($r);

// $r = updateGroup($host, "59369d11c39de75980ff0c0b", [
//     'auth_token' => $token,
//     'name' => 'gruppo_1',
//     'sites_allowed' => [
//         'http://test.it',
//         'http://test2.it'
//     ]
// ]);
// dd($r);

// $r = getGroup($host, $token, "59369d11c39de75980ff0c0b");
// dd($r);

// $r = deleteGroup($host, $token, "59369d11c39de75980ff0c0b");
// dd($r);

// $r = listGroups($host, $token);
// dd($r);
