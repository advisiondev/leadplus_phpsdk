<?php

namespace Advision\Fog\Models;

class Recipient extends Model
{
    protected $name;

    protected $fields;

    protected function factoryClass()
    {
        return \Advision\Fog\Factories\Recipient::class;
    }

    public function getEndpoint()
    {
        return 'list';
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param mixed $fields_list
     *
     * @return self
     */
    public function setFields($fields)
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id'          => $this->getId(),
            'name'        => $this->getName(),
            'fields_list' => $this->getFields()
        ];
    }

    /**
     * @return json
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }
}