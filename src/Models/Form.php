<?php

namespace Advision\Fog\Models;

class Form extends Model
{
    protected $name;

    protected $groupId;

    protected $recipientId;

    protected $foreignRecipientId;
    protected $foreignIntegrationId;

    protected $foreignUserId;

    protected $updateIfDuplicate;

    protected $active;

    protected $theme;

    protected $actionUrl;

    protected $schedule;

    protected function factoryClass()
    {
        return \Advision\Fog\Factories\Form::class;
    }

    public function getEndpoint()
    {
        return 'form';
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param string $groupId
     *
     * @return self
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRecipientId()
    {
        return $this->recipientId;
    }

    /**
     * @param mixed $recipientId
     *
     * @return self
     */
    public function setRecipientId($recipientId)
    {
        $this->recipientId = $recipientId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getForeignRecipientId()
    {
        return $this->foreignRecipientId;
    }

    /**
     * @param mixed $foreignRecipientId
     *
     * @return self
     */
    public function setforeignRecipientId($foreignRecipientId)
    {
        $this->foreignRecipientId = $foreignRecipientId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getForeignUserId()
    {
        return $this->foreignUserId;
    }

    /**
     * @param mixed $foreignUserId
     *
     * @return self
     */
    public function setForeignUserId($foreignUserId)
    {
        $this->foreignUserId = $foreignUserId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getForeignIntegrationId()
    {
        return $this->foreignIntegrationId;
    }

    /**
     * @param mixed $foreignIntegrationId
     *
     * @return self
     */
    public function setForeignIntegrationId($foreignIntegrationId)
    {
        $this->foreignIntegrationId = $foreignIntegrationId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdateIfDuplicate()
    {
        return $this->updateIfDuplicate;
    }

    /**
     * @param mixed $updateIfDuplicate
     *
     * @return self
     */
    public function setUpdateIfDuplicate($updateIfDuplicate)
    {
        $this->updateIfDuplicate = $updateIfDuplicate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     *
     * @return self
     */
    public function setActive($active)
    {
        $this->active = (bool)$active;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActionUrl()
    {
        return $this->actionUrl;
    }

    /**
     * @param mixed $actionUrl
     *
     * @return self
     */
    public function setActionUrl($actionUrl)
    {
        $this->actionUrl = $actionUrl;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id'            => $this->getId(),
            'name'          => $this->getName(),
            'foreign_user_id'          => $this->getForeignUserId(),
            'foreign_recipient_id'          => $this->getForeignRecipientId(),
            'foreign_integration_id'          => $this->getForeignIntegrationId(),
            'update_if_duplicate'          => $this->getUpdateIfDuplicate(),
            'schedule'      => $this->getSchedule(),
            'formgroup_id'  => $this->getGroupId(),
            'formlist_id'   => $this->getRecipientId(),
            'isActive'      => $this->getActive(),
            'actionUrl'     => $this->getActionUrl(),
            'theme'         => $this->getTheme()
        ];
    }

    /**
     * @return json
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }

    /**
     * @return mixed
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @param mixed $theme
     *
     * @return self
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * @param mixed $schedule
     *
     * @return self
     */
    public function setSchedule($schedule)
    {
        $this->schedule = $schedule;

        return $this;
    }
}

