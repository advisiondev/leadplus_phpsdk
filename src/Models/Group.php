<?php

namespace Advision\Fog\Models;

class Group extends Model
{
    protected $name;

    protected $sites = [];

    protected $totalForms;

    protected $totalSubscribers;

    protected $forms = [];

    protected function factoryClass()
    {
        return \Advision\Fog\Factories\Group::class;
    }

    public function getEndpoint()
    {
        return 'group';
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSites()
    {
        return $this->sites;
    }

    /**
     * @param array $sites
     *
     * @return self
     */
    public function setSites(array $sites)
    {
        $this->sites = $sites;

        return $this;
    }

    /**
     * @param string $site
     *
     * @return self
     */
    public function addSite($site)
    {
        if (array_search($site, $this->sites) === false)
        {
            $this->sites[] = $site;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getForms()
    {
        return $this->forms;
    }

    /**
     * @return array
     */
    public function setForms($forms)
    {
        $this->forms = $forms;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalForms()
    {
        return $this->totalForms;
    }

    /**
     * @param mixed $totalForms
     *
     * @return self
     */
    public function setTotalForms($totalForms)
    {
        $this->totalForms = $totalForms;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalSubscribers()
    {
        return $this->totalSubscribers;
    }

    /**
     * @param mixed $totalSubscribers
     *
     * @return self
     */
    public function setTotalSubscribers($totalSubscribers)
    {
        $this->totalSubscribers = $totalSubscribers;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id'          => $this->getId(),
            'name'        => $this->getName(),
            'sites_allowed' => $this->getSites(),
            'forms'       => $this->getForms(),
            'total_subscribers' => $this->getTotalSubscribers(),
        ];
    }

    /**
     * @return json
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }
}