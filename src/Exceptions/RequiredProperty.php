<?php

namespace Advision\Fog\Exceptions;

class RequiredProperty extends \Exception
{
    public function __construct($property)
    {
        $message = 'The property %s must be set';

        parent::__construct(sprintf($message, $property));
    }
}