<?php

namespace Advision\Fog\Exceptions;

class NoPropertyFound extends \Exception
{
    public function __construct($property, $class)
    {
        $message = 'The property %s must be exist in %s';

        parent::__construct(sprintf($message, $property, $class));
    }
}