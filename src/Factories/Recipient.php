<?php

namespace Advision\Fog\Factories;

use Advision\Fog\Models\Recipient as RecipientModel;

class Recipient extends SingletonFactory
{
    public static function getDefaultProperties() {
        return [
            '_id'                   => null,
            'integration_id'        => null,
            'integration_user_id'   => null,
            'name'                  => '',
            'fields_list'           => []
        ];
    }
    public static function fromObject($data)
    {
        $data = objectToArray($data);

        return self::fromArray($data);
    }

    public static function fromArray(array $data)
    {
        $properties = array_replace_recursive(self::getDefaultProperties(), $data);

        $model = new RecipientModel;

        $model->setId($properties['_id'])
              ->setName($properties['name'])
              ->setFields($properties['fields_list'])
              ->setIntegrationId($properties['integration_id'])
              ->setIntegrationUserId($properties['integration_user_id']);

        return $model;
    }
}
