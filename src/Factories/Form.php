<?php

namespace Advision\Fog\Factories;

use Advision\Fog\Models\Form as FormModel;

class Form extends SingletonFactory
{
    public static function getDefaultProperties() {
        return [
            '_id'                   => null,
            'integration_id'        => null,
            'integration_user_id'   => null,
            'foreign_user_id'   => null,
            'foreign_recipient_id'   => null,
            'foreign_integration_id'   => null,
            'update_if_duplicate'   => null,
            'name'                  => '',
            'schedule'              => null,
            'formgroup_id'          => null,
            'formlist_id'           => null,
            'isActive'              => false,
            'actionUrl'             => null,
            'theme'                 => [
                'type' => 'embedded',
                'name' => 'fog_base',
            ],
        ];
    }

    public static function fromObject($data)
    {
        $data = objectToArray($data);

        return self::fromArray($data);
    }

    public static function fromArray(array $data)
    {
        $properties = array_replace_recursive(self::getDefaultProperties(), $data);

        $model = new FormModel;

        $model->setId($properties['_id'])
              ->setName($properties['name'])
              ->setIntegrationId($properties['integration_id'])
              ->setForeignRecipientId($properties['foreign_recipient_id'])
              ->setForeignUserId($properties['foreign_user_id'])
              ->setForeignIntegrationId($properties['foreign_integration_id'])
              ->setUpdateIfDuplicate($properties['update_if_duplicate'])
              ->setGroupId($properties['formgroup_id'])
              ->setRecipientId($properties['formlist_id'])
              ->setActive((bool)$properties['isActive'])
              ->setActionUrl($properties['actionUrl'])
              ->setTheme($properties['theme'])
              ->setSchedule($properties['schedule'])
              ->setIntegrationUserId($properties['integration_user_id']);

        return $model;
    }
}
