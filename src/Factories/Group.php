<?php

namespace Advision\Fog\Factories;

use Advision\Fog\Models\Group as GroupModel;

class Group extends SingletonFactory
{
    public static function getDefaultProperties() {
        return [
            '_id'                   => null,
            'integration_id'        => null,
            'integration_user_id'   => null,
            'total_forms'           => 0,
            'total_subscribers'     => 0,
            'name'                  => '',
            'sites_allowed'         => [],
            'forms'                 => []
        ];
    }
    public static function fromObject($data)
    {
        $data = objectToArray($data);

        return self::fromArray($data);
    }

    public static function fromArray(array $data)
    {
        $properties = array_replace_recursive(self::getDefaultProperties(), $data);

        $model = new GroupModel;

        $model->setId($properties['_id'])
              ->setName($properties['name'])
              ->setSites($properties['sites_allowed'])
              ->setIntegrationId($properties['integration_id'])
              ->setTotalForms($properties['total_forms'])
              ->setTotalSubscribers($properties['total_subscribers'])
              ->setForms($properties['forms'])
              ->setIntegrationUserId($properties['integration_user_id']);

        return $model;
    }
}
