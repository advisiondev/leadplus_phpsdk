<?php

namespace Advision\Fog\Contracts;

interface Jsonable
{
    public function toJson();
}