<?php

namespace Advision\Fog\Contracts;

interface Arrayable
{
    public function toArray();
}