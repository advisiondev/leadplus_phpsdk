<?php

namespace Advision\Fog;

use Advision\Fog\Models\Model;
use Advision\Fog\Factories\Group as GroupFactory;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Advision\Fog\Exceptions\NoTokenProvided;
use Advision\Fog\Exceptions\ResponseError;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Client as GuzzleClient;

class Client
{
    protected static $host = 'https://api.formplus.it';

    protected $token;

    protected $client;

    protected $version = 'v1';

    public function __construct($token, $host = null)
    {
        if (!$token)
        {
            throw new NoTokenProvided();
        }

        $this->token = $token;

        if ($host)
        {
            self::$host = $host;
        }

        $this->setupClient();
    }

    public function find(Model $model)
    {
        try {
            $res = $this->client->request('get', $this->getEndpoint($model));
            if (is_array($res['body'])) {
                return $model->getFactory()->fromCollections($res['body']);
            }
            return $model->getFactory()->fromObject($res['body']);

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function save(Model $model)
    {
        if ($model->getId())
        {
            return $this->update($model);
        } else {
            return $this->create($model);
        }
    }

    public function delete(Model $model)
    {
        try {

            $res = $this->client->request('delete', $this->getEndpoint($model));

            return $res['body'];

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getClient()
    {
        return $this->client;
    }

    protected function create(Model $model)
    {
        try {

            $data = $model->toArray();

            unset($data['id']);

            $res = $this->client->request('post', $this->getEndpoint($model), [
                'json' => $data
            ]);

            return $model->getFactory()->fromObject($res['body']);

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    protected function update(Model $model)
    {
        try {

            $data = $model->toArray();

            $res = $this->client->request('put', $this->getEndpoint($model), [
                'json' => $data
            ]);

            return $model->getFactory()->fromObject($res['body']);

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    protected function getEndpoint(Model $model)
    {
        return $this->version . '/' . $model->getEndpoint() . '/' . ($model->getId() ? $model->getId() : '') . '/' . ($model->emptyRelateds() ? '': $model->getRelateds());
    }

    protected function setupClient()
    {
        $stack = new HandlerStack();

        $stack->setHandler(\GuzzleHttp\choose_handler());

        $stack->push(Middleware::mapRequest(function (RequestInterface $r) {
            return $r->withHeader('Authorization', 'JWT '.$this->token);
        }, 'token'));

        $stack->push(Middleware::mapResponse(function (ResponseInterface $response) {

            if ($response->getStatusCode() >= 200 && $response->getStatusCode() <= 300)
            {
                return [
                    'headers' => $response->getHeaders(),
                    'body' => json_decode($response->getBody()->getContents()),
                    'code' => $response->getStatusCode()
                ];
            }

            $data = json_decode($response->getBody()->getContents());

            throw new ResponseError($response->getStatusCode(), $data->error, $data->messages);

        }));

        $config = [
            'base_uri' => self::$host,
            'handler' => $stack
        ];

        $this->client = new GuzzleClient($config);
    }
}